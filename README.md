INTRODUCTION
------------

The Flockler Feed module provides a block to display a feed from https://flockler.com/

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

CONFIGURATION
-------------

 * Go to Administration » Structure » Blocks
 * In the region where you want the feed to appear, click "Place block"
 * Select "Flockler Block"
 * In the block configuration form, enter the URL of the feed, e.g. https://api.flockler.com/v1/sites/1111/articles
