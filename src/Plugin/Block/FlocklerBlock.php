<?php

namespace Drupal\flockler\Plugin\Block;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Html;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use GuzzleHttp\ClientInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'FlocklerBlock' block.
 *
 * @Block(
 *  id = "flockler_block",
 *  admin_label = @Translation("Flockler block"),
 * )
 */
class FlocklerBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Guzzle Http Client.
   *
   * @var GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ClientInterface $http_client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->httpClient = $http_client;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['url'] = [
      '#type' => 'url',
      '#title' => $this->t('Feed URL'),
      '#description' => $this->t('The URL of the Flockler feed'),
      '#default_value' => $this->configuration['url'],
      '#maxlength' => 64,
      '#required' => TRUE,
      '#size' => 64,
      '#weight' => '0',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['url'] = $form_state->getValue('url');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['content'] = $this->fetchFeed();
    $build['#attributes'] = [
      'class' => [
        'flockler-articles',
      ],
    ];
    return $build;
  }

  private function fetchFeed() {
    $url = $this->configuration['url'];

    $response = $this->httpClient->get($url);
    $body = $response->getBody()->__toString();
    $jsonObject = Json::decode($body, true);

    // TODO: cache contexts.
    return $this->formatFeed($jsonObject);
  }

  private function formatFeed($jsonObject) {
    $render = [];

    if (!empty($jsonObject)) {
      $render = [
        '#attributes' => [
          'class' => [
            'flockler-articles',
          ],
        ],
      ];

      foreach ($jsonObject['articles'] as $article) {
        $render[] = $this->formatArticle($article);
      }
    }
    return $render;
  }

  /**
   * Prepare an article
   *
   * @param array $article
   *   The JSON representation of the article in the feed.
   *
   * @return array
   *   A render array.
   */
  private function formatArticle($article) {
    $render = [];

    switch ($article['type']) {
      case 'tweet':
        $render = $this->formatTweet($article);
        break;
      case 'instagram':
        $render = $this->formatInstagram($article);
        break;
    }

    return $render;
  }

  /**
   * Prepare a render array for a tweet.
   *
   * @param array $article
   *   The JSON representation of the tweet.
   *
   * @return array
   *   A render array for the tweet.
   */
  private function formatTweet($article) {

    $tweet = $article['attachments']['tweet'];

    $render = [
      '#theme' => 'flockler_article',
      '#content' => [
        'type' => 'tweet',
        'profile_image' => $tweet['profile_image_url'],
        'profile_name' => $tweet['name'],
        'profile_link' => 'https://twitter.com/' . $tweet['screen_name'],
        'text' => Html::decodeEntities($tweet['text']),
        'image' => $tweet['media_url'],
        'link' => 'https://twitter.com/' . $tweet['screen_name'] . '/status/' . $tweet['tweet_id_str'],
        'created_time' => strtotime($tweet['created_at']),
      ],
    ];

    if (!$this->isImage($tweet['media_url'])) {
      $videoPreview = $this->getVideoThumbnail($tweet['media_url']);
      if ($videoPreview) {
        $render['#content']['image'] = $videoPreview;
      }
    }

    return $render;
  }

  /**
   * Prepare a render array for an instagram post.
   *
   * @param array $article
   *   The JSON representation of the post.
   *
   * @return array
   *   A render array for the post.
   */
  private function formatInstagram($article) {
    $post = $article['attachments']['instagram_item'];

    $render = [
      '#theme' => 'flockler_article',
      '#content' => [
        'type' => 'instagram',
        'text' => Html::decodeEntities($post['caption']),
        'profile_image' => $post['profile_picture'],
        'profile_name' => $post['user_full_name'],
        'profile_link' => 'https://www.instagram.com/' . $post['username'],
        'link' => $post['link'],
        'image' => $article['cover_url'],
        'created_time' => strtotime($post['created_time']),
      ],
    ];
    return $render;
  }

  /**
   * Check if a URL refers to an image.
   *
   * @param string $url
   *   The media URL to check.
   *
   * @return bool
   *   TRUE if the URL refers to an image.
   */
  private function isImage($url) {
    $urlParts = explode('.', $url);
    $extension = end($urlParts);

    $imageTypes = [
      'jpg',
      'jpeg',
      'png',
      'gif',
      'svg',
    ];

    return in_array(strtolower($extension), $imageTypes);
  }

  /**
   * Get the thumbnail for a video.
   *
   * @param string $url
   *   The media URL from the article.
   *
   * @return string
   *   The URL of the video thumbnail.
   */
  private function getVideoThumbnail($url) {
    $imageUrl = '';

    if (!empty($url)) {
      // Fetch the page.
      $response = $this->httpClient->get($url);
      $body = $response->getBody()->__toString();

      // Does the page have the twitter:image metatag? If so, use it.
      $html = Html::load($body);
      $metas = $html->getElementsByTagName('meta');
      foreach ($metas as $meta) {
        $name = $meta->getAttribute('name');
        if ($name == 'twitter:image') {
          $imageUrl = $meta->getAttribute('content');;
        }
      }
    }

    return $imageUrl;
  }
}
